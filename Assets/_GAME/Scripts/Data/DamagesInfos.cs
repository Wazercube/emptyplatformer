﻿///<summary>
/// Represents the loss of one or more lives.
///</summary>
[System.Serializable]
public struct DamagesInfos
{
    public int livesLost;
    public int remainingLives;
}