﻿using UnityEngine;

///<summary>
/// Contains respawn informations.
///</summary>
[System.Serializable]
public struct SpawnInfos
{
	public Vector3 lastPosition;
	public Vector3 spawnPosition;
}