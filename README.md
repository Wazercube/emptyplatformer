# Empty Platformer project

A platformer made with Unity, with basic mechanics. This repository is for education purposes, so you can use it as a template for you projects.

The main goal of this project is to add as much sign and feedbacks as possible to make a very simple platformer with only the basic moves the most insanely pleaseful to play.

For this exercise, the content is not useful. It's just a test environment with a static camera.

## Rules and allowed moves



### Player

Mechanics:

- Movement
- Jump
- Shoot
- Life points
- Scoring

#### Actions

- Move (left or right)
- Jump
- Shoot (in its movement direction or by using mouse to aim)

**Shoot** action has a cooldown. The duration of that cooldown is up to you. Think about what kind of weapon you want to give to your player ;)

#### Life points

The player has 3 life points. It loses one when it touches an enemy.

You don't need to display life points on UI, but the current health of the player must be showed to him.

#### Score

The score is increased each time an enemy is killed or a collectible is get.

### Enemy action

- Moves from a side of its platform to another
- ... Nothing else.

## Reference games

- [Overwhelm](https://www.youtube.com/watch?v=yto8PGUWf8U)
- [Ori and the Blind Forest](https://www.youtube.com/watch?v=cklw-Yu3moE)
- [Hollow Knight](www.youtube.com/watch?v=UAO2urG23S4)
- [Hollow Knight (with AK-47 Mod)](https://youtu.be/bq6OJdNcJAw?t=399)
- [King of the Hat](https://www.youtube.com/watch?v=4khBEOIwjyQ)
- [Celeste](https://www.youtube.com/watch?v=70d9irlxiB4)
- [Nuclear Throne](https://www.youtube.com/watch?v=7LSs1bj41P4)
- [Syntetik](https://www.youtube.com/watch?v=v5-EYFkf-KU)

## Other references

- https://twitter.com/BsMcInnes/status/1228772415683153920